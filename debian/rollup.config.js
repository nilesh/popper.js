module.exports = {
  input: "./packages/popper/src/index.js",
  plugins: [require("rollup-plugin-buble")({
      objectAssign: 'Object.assign'
  })],
  output: {
    format: 'es',
    file: './dist/popper.js'
  }
};
