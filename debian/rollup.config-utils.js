module.exports = {
  input: "./packages/popper/src/utils/index.js",
  plugins: [require("rollup-plugin-buble")({
      objectAssign: 'Object.assign'
  })],
  output: {
    format: 'es',
    file: './dist/popper-utils.js'
  }
};
